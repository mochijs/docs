---
title: Content - Assets & Easy Async Loading
weight: 7
---

The Asset class is used to load any kind of data -- image or JSON.
It is used for content management in our engine.

## Asset Class

A classic example of the asset class is using it to load images.

```javascript
let asset = new Mochi.Content.Asset("assets/aPicture.png");
```

You can use this asset to do asynchronous loading with the promises API.

```javascript
let asset = new Mochi.Content.Asset("assets/aPicture.png");
asset.load().then(function(data) {
    console.log(data);
});
```

## Loading Multiple Assets

You can also use Promise.All or our Asset class loader to load multiple objects.

```javascript
let asset1 = new Mochi.Content.Asset("assets/aPicture1.png");
let asset2 = new Mochi.Content.Asset("assets/aPicture2.png");

// resolves when all of the assets are loaded or failed to load.
Asset.LoadAllAssets([asset1, asset2]).then(function(summary) {
    // shows a summary of what loaded and what didn't.
    console.log(summary);
});
```