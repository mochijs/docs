---
title: Entities, Components, Managers
weight: 6
---

The Mochi Architecture is built around three core classes - Entity, Component, and Manager.



## Entity

Entities are the core of the Mochi engine. All game objects are entities.
Entities have components added to them. These components add data and features that are processed by managers.

Creating an entity is easy:
```javascript
let entity = new Mochi.Entity(0, 0);
```

{{% notice tip %}}
We recommend using the Factory pattern to create entities!
{{% /notice %}}

Technically, in an ideal world, you would never need to extend Entity. Just add new components that can be mixed and matched onto entity.

## Components

Components are attached to Entities. They provide additional data that is processed by Managers.

The Mochi Engine comes with several components that will be displayed throughout these tutorials, but for example, here is a Sprite component.
```javascript
let char = new Entity(0, 0);
// load an asset
let asset = new Mochi.Content.Asset("assets/myChar.png");
// adds this component to this entity
char.add(new Mochi.Graphics.Sprite(asset, {width: 1, height: 1}));
char.init();
```

## Managers

Managers are Managed by the Scene class. They perform operations on the data provided in Entity.
