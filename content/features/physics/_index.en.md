---
title: Physics - Collision & Particle Systems
weight: 9
---

## Collision

The Body class is used to implement collision bounding boxes that can be applied to entities.
This class also adds in acceleration and velocity for convenience, and updates them when modified. Forces can be applied.

The Body class accepts several different types of shapes for collision:
* Circle
* Axis Aligned Bounding Box
* Oriented Bounding Box (if rotation is applied)

## Particle Systems

The particle system is in two parts:
 * Emitter, the component attached that will spawn entities based on an archetype
 * EmitterManager, a manager that updates all entities with an emitter.

The Emitter is a component added to an entity. It accepts an archetype and settings that allow for infinite
modifications and variations on any data added to entity.

Here's an example Emitter from Asteroids:

```javascript
function makeAsteroidSpawner(game, factory, spawnRate) {
    let entity = new Mochi.Entity(game.width/2, game.height/2);
    entity.add(new Mochi.Physics.Emitter(
        // archetype of the object
        {
            // optional specifier for variation amt, otherwise uses what already exists from factory
            x: new Mochi.Physics.Properties.Range(0, game.width),
            y: new Mochi.Physics.Properties.Range(0, game.height),
            rotation: new Mochi.Physics.Properties.Range(0, 2 * Math.PI),
            body: {
                speedX: new Mochi.Physics.Properties.Range(-5, 5),
                speedY: new Mochi.Physics.Properties.Range(-5, 5),
                shape: {
                    size: new Mochi.Physics.Properties.Custom(new Mochi.Physics.Properties.Range(1, 3), function (entity, parent, generated) {
                        entity.body.shape.radius = generated / 2;
                        entity.sprite.width = generated;
                        entity.sprite.height = generated;
                    })
                }
            }
        },
        // configuration setters of the emitter
        {
            //spawnCount: new Mochi.Physics.Properties.Range(0, 2),
            // required set to 3 with 20% variation off of 3
            spawnTime: new Mochi.Physics.Properties.Variation(.3, spawnRate),
            //spawnMax: 10,
            // optional decay
            //decayTime: new Variation(.4),
        },
        factory
    ));
    entity.init();
    return entity;
}
```
