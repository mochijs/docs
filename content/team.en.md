---
title: Team
disableToc: true
---

## Developers

Thanks for making the Mochi Engine possible <i class="fa fa-heart"></i>!

 * **Kenny Jones** (kgjones3)
 * **Justin Patzer** (jepatzer)
 * **Robby Seligson** (rfseligs)
 
 
And a special thanks to our instructor, Arnav Jhala, for providing such a flexible assignment this semester.

## Packages and Libraries
* [RequireJS](http://requirejs.org/) - This library pulls the entire engine together since ES6 import/export modules have not been standardized in all browsers.
* [NodeJS](https://nodejs.org/en/) - Included inside the engine for both package management (npm) and for networking.
* [Socket.io](https://socket.io/) - For socket communication for client-server and p2p.
* [Express](https://expressjs.com/) - For webserver set up and management.
* [font awesome](http://fontawesome.io/) - the iconic font and CSS framework for documentation.

## Tooling

* [Hugo Learn Theme](https://github.com/matcornic/hugo-theme-learn) - Wonderful documentation template tool.
* [Hugo](https://gohugo.io/) - For our wonderful documentation to be generated.

