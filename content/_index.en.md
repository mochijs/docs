---
title: "Mochi Engine Documentation"
---

# Mochi Engine

[The Mochi Engine](https://gitlab.com/csc481/engine) is an ES6-standard 2D JavaScript Game Engine for HTML5 canvas that uses an Entity-Component-Manager architecture. 
The engine focuses on **flexibility** over _structure_, **convenience** over _stricture_, and **composition** over _inheritance_. 

The Mochi Engine can be used like a general purpose framework. We provide components that work together and standalone. 

## Main features

* [Game & State Management]({{%relref "features/game/_index.md" %}})
* [Entities, Components, Managers]({{%relref "features/entities/_index.md" %}})
* [Content - Assets & Easy Async Loading]({{%relref "features/content/_index.md" %}})
* [Graphics - Sprites, User Interface & Animation ]({{%relref "features/graphics/_index.md" %}})
* [Physics - Collision & Particle Systems]({{%relref "features/physics/_index.md" %}})
* [Input - Callback or State]({{%relref "features/input/_index.md" %}})
* [Event Handling - The Event Bus]({{%relref "features/events/_index.md" %}})
* [Multiplayer & Networking]({{%relref "features/networking/_index.md" %}})
* [Artificial Intelligence - Pathfinding]({{%relref "features/ai/_index.md"%}})

![Logo](images/mochi_logo_cream.png)

## Development
The engine is currently on-going active development. Architectural changes are not likely to change, but most everything else is. Be warned!

{{% notice info %}}
The Mochi Engine is currently in **alpha** development.
{{% /notice %}}

## Contribution
The Mochi engine development team would love to have your contributions! Check out our gitlab repo, and see the docs for additional information.
