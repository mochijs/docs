---
title: Download & Setup
weight: 5
---

The following steps are here to help you start development with the Mochi Engine.

## Download the Mochi Engine

Simply clone the engine repository's source.

```
git clone https://gitlab.com/csc481/engine.git
```

{{% notice note %}}
You'll need to reference the location from your game. Put it near your game!
{{% /notice %}}

## Install Dependencies _(Networked Games Only)_
If you plan to use the Mochi engine for networked games, you'll need to install additional dependencies.

First, you'll need to install [nodejs](https://nodejs.org/en/).
The Mochi engine is set up to be used within Node, but doesn't come pre-downloaded with all the necessary components.

Simply use the node package manager to install the dependencies within the engine:

```
// From within the engine directory
npm install
```
