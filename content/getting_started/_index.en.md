---
title: Getting Started
weight: 1
pre: "<b>1. </b>"
chapter: true
---

### Chapter 1

# Getting Started

Learn how to set up and configure the Mochi Engine and get straight into game development!
